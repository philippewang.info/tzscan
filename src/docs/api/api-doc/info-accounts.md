
### Sample Result

List of accounts in JSON

```json
[
  {
    "hash": "TZ1dR5zkRparEAVvyjNF4xactuavFQ7sYdqy",
    "counter": 3,
    "manager": "tz1a7kVfHfmwW4YroFP6483eLA8vMkxrfEBP",
    "delegate": "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx",
    "spendable": true,
    "delegatable": true,
    "balance": 10000,
    "operations": ["oouRh4AuQXMpjdagfAobPhKCXDxqKV1tN5hAWiFKKpbJ7YdC2V5"]
  }
]
```
