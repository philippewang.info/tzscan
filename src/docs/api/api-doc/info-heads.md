### Sample result

```json
[
  {
    "hash":"BL6r4HEQVDudB8xRoE1GiVYic4XuvP1G15JABE9THgmPq5c1nhR",
    "predecessor_hash":"BKzzd4sH34NWRymjEosNNP1jzvQeVAEu9pecjzAsZbiGgoLaZjS",
    "fitness":"00 000000000007e492",
    [...]
  },
  {
    "hash":"BMPBybcgWaG4uTkSZ3oycRxMpjYfdyMv2C3sy5r2rHrpawL94vf",
    "predecessor_hash":"BMPteHPnRxtKXYWxt8A4w9C2JMiMk2gaFwnsjZx93Z8zxq2o2Qk",
    "fitness":"00 000000000007e454",
    [...]
  },
  {
    "hash":"BLsywNtXqRZp3cV7F2uEkn7tXjz5g6J39ybrHP5X2DwpFvHf2Pp",
    "predecessor_hash":"BMPteHPnRxtKXYWxt8A4w9C2JMiMk2gaFwnsjZx93Z8zxq2o2Qk",
    "fitness":"00 000000000007e45c",
    [...]
  },
  {
    "hash":"BLGi4wKSL9PEPGwLAK7aKhkKwa3q9dhx4mZh2KLgJbYDW5Efuji",
    "predecessor_hash":"BLEJsqC5py9NWs6GdoZkzw5MM7DRdyaBMxXyqwtp7gac1owcsDu",
    "fitness":"00 000000000007e44b",
    [...]
  },
  {
    "hash":"BMVQ3ujNct6YDhwyuzRGPXCk4ZBpwyB3WtMQFGFrCw5a9KQjAEk",
    "predecessor_hash":"BMFFDcfeEBNEf1XCgyNUi5XaaQefs2iGMj5DV8rkdsPozXRL5Tj",
    "fitness":"00 000000000007e597",
    [... same as /block/<blockhash>?operations=false ... ]
  },
  [...]
]
```
