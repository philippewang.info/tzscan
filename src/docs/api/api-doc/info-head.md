### Sample result

```json
{
   "hash":"BMVQ3ujNct6YDhwyuzRGPXCk4ZBpwyB3WtMQFGFrCw5a9KQjAEk",
   "predecessor_hash":"BMFFDcfeEBNEf1XCgyNUi5XaaQefs2iGMj5DV8rkdsPozXRL5Tj",
   "fitness":"00 000000000007e597",
   "timestamp":"2018-02-07T14:29:10Z",
   [... same as /block/<blockhash>?operations=false ... ]
}
```

